﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DesafioConcrete;

namespace UnitTestProjectDesafio
{
    [TestClass]
    public class UnitTest1
    {
        private Carrinho carrinho;

        private Item itemA = new Item {Nome = "A", Preco = 50 };
        private Item itemB = new Item { Nome = "B", Preco = 30 };
        private Item itemC = new Item { Nome = "C", Preco = 20 };
        private Item itemD = new Item { Nome = "D", Preco = 15 };
        private Desconto descontoItemA = new Desconto {ItemNome = "A", ItemQuantidade = 3, Total = 130 };
        private Desconto descontoItemB = new Desconto { ItemNome = "B", ItemQuantidade = 2, Total = 45 };


        [TestInitialize]
        public void SetUp()
        {
            carrinho = new Carrinho();
        }

        [TestMethod]
        public void Teste_Carrinho_Vazio_Retorna_Zero()
        {
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(0, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_Um_ItemA_Total()
        {
            AdicionarItens(1, itemA);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(50, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_ItemA_E_ItemB_Total()
        {
            AdicionarItens(1, itemA);
            AdicionarItens(1, itemB);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(80, resultado);
        }


        [TestMethod]
        public void Teste_Carrinho_ItemC_E_ItemD_E_ItemB_E_ItemA_Total()
        {
            AdicionarItens(1, itemC);
            AdicionarItens(1, itemD);
            AdicionarItens(1, itemB);
            AdicionarItens(1, itemA);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(115, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_ItemA_E_ItemA_Total()
        {
            AdicionarItens(2, itemA);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(100, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_ItemA_Desconto_Total()
        {
            carrinho.AdicionarDesconto(descontoItemA);
            AdicionarItens(3, itemA);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(130, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_Quatro_ItensA_Desconto_Total()
        {
            carrinho.AdicionarDesconto(descontoItemA);
            AdicionarItens(4, itemA);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(180, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_Cinco_ItensA_Desconto_Total()
        {
            carrinho.AdicionarDesconto(descontoItemA);
            AdicionarItens(5, itemA);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(230, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_Seis_ItensA_Desconto_Total()
        {
            carrinho.AdicionarDesconto(descontoItemA);
            AdicionarItens(6, itemA);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(260, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_Tres_ItensA_E_ItemB_Desconto_Total()
        {
            carrinho.AdicionarDesconto(descontoItemA);
            AdicionarItens(3, itemA);
            AdicionarItens(1, itemB);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(160, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_Tres_ItensA_E_Dois_ItensB_Desconto_Total()
        {
            carrinho.AdicionarDesconto(descontoItemA);
            carrinho.AdicionarDesconto(descontoItemB);
            AdicionarItens(3, itemA);
            AdicionarItens(2, itemB);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(175, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_Tres_ItensA_E_Dois_ItensB_E_ItemD_Desconto_Total()
        {
            carrinho.AdicionarDesconto(descontoItemA);
            carrinho.AdicionarDesconto(descontoItemB);
            AdicionarItens(3, itemA);
            AdicionarItens(2, itemB);
            AdicionarItens(1, itemD);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(190, resultado);
        }

        [TestMethod]
        public void Teste_Carrinho_Tres_ItensA_E_Dois_ItensB_E_ItemD_Diversos_Desconto_Total()
        {
            carrinho.AdicionarDesconto(descontoItemA);
            carrinho.AdicionarDesconto(descontoItemB);
            AdicionarItens(1, itemD);
            AdicionarItens(1, itemA);
            AdicionarItens(1, itemB);
            AdicionarItens(1, itemA);
            AdicionarItens(1, itemB);
            AdicionarItens(1, itemA);
            int resultado = carrinho.CalcularTotal();
            Assert.AreEqual(190, resultado);
        }


        private void AdicionarItens(int quantidade, Item item)
        {
            for (int i = 0; i < quantidade; i++)
            {
                carrinho.AdicionarItem(item);
            }

        }

    }
}
