﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioConcrete
{
    public class Item
    {
        public string Nome { get; set; }
        public int Preco { get; set; }
    }
}
