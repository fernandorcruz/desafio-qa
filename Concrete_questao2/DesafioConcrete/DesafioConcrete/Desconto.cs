﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioConcrete
{
    public class Desconto
    {
        public string ItemNome { get; set; }
        public int ItemQuantidade { get; set; }
        public int Total { get; set; }
    }
}
