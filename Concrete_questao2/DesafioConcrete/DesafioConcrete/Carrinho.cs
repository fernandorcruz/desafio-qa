﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioConcrete
{
    public class Carrinho
    {
        private readonly List<Item> listaItens = new List<Item>();
        private readonly List<Desconto> listaDescontos = new List<Desconto>();


        public int CalcularTotal()
        {
            int total = 0;
            var grupoItens = listaItens.GroupBy(x => x.Nome);

            foreach (var grupo in grupoItens)
            {
                var descontoPorGrupo = listaDescontos.FirstOrDefault(x => x.ItemNome == grupo.Key);
                if (descontoPorGrupo != null)
                {
                    var qtdGrupo = grupo.Count();
                    var qtdAcimaDoDesconto = qtdGrupo - descontoPorGrupo.ItemQuantidade;

                    if (qtdAcimaDoDesconto < 0 )
                    {
                        total += grupo.Sum(x => x.Preco);
                    }
                    else if (qtdGrupo % 3 == 0 && grupo.Key == "A") //Aplica desconto A
                    {
                        total += (qtdGrupo / 3) * descontoPorGrupo.Total;
                    }
                    else if (qtdGrupo % 2 == 0 && grupo.Key == "B") //Aplica desconto B
                    {
                        total += (qtdGrupo / 2) * descontoPorGrupo.Total;
                    }
                    else
                    {
                        total += descontoPorGrupo.Total;
                        total += qtdAcimaDoDesconto * grupo.First().Preco;
                    }

                }
                else
                {
                    total += grupo.Sum(x => x.Preco);
                }
            }
            return total;

        }

        public void AdicionarDesconto(Desconto desconto)
        {

            if (desconto == null)
            {
                throw new ArgumentNullException();
            }
            else if (string.IsNullOrWhiteSpace(desconto.ItemNome))
            {
                throw new ArgumentException();
            }
            else
            {
                listaDescontos.Add(desconto);
            }

        }

        public void AdicionarItem(Item item)
        {
            listaItens.Add(item);
        }

        



    }
}
